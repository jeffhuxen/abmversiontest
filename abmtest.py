
# import the required libraries
import smtplib
from email.message import EmailMessage
import mysql.connector

# variables to store static addresses
email_from = "updates@cellcontrol.com"
email_list = ["jeff.huxen@trucesoftware.com", 
                "john.wright@trucesoftware.com",
                "jake.wagner@trucesoftware.com",
                "steve.mckinney@trucesoftware.com",
                "joe.breaux@trucesoftware.com",
                "bradston.henry@trucesoftware.com",
                "chad.howell@trucesoftware.com"]
# email subject warning message
email_subject = "WARNING! CRITICAL! ABM VERSION CONFLICT!"

# connect to mysql database
mydb = mysql.connector.connect(
  host="cellcontroldb.c9bzjgdpzwkq.us-east-1.rds.amazonaws.com",
  user="reader",
  passwd="2EC5F41E-CE2F-49CE-A469-4E315C13EEC7",
  database="obdedge")
# stored static SQL query
sql = "select phone.entityID, phone.id, phone.version, entity_features.ios_pushlimits from phone join entity_features on entity_features.entityID = phone.entityID where version like '13.%' && length(version) = 6 && phone.id != 'apple' && ios_pushlimits != '11311000' group by phone.entityID"

class ABMVersionCheck:

    # function to create failed block for each failure
    def createFailureMessage(self, entityId, phoneNum, version):    
        body = """
        Failed abm version check for:

        Entity ID: %s,
        Phone Number: %s,
        Version: %s
        """ % (entityId, phoneNum, version)
        return body


    # function to send email warnings to members of list
    def sendWarning(self, results):

        # create a new email message and set its properties
        msg = EmailMessage()
        msg['Subject'] = email_subject
        msg['From'] = email_from
        msg['To'] = email_list
        email_body = """
        An ABM misconfiguration has been detected.  
        A user is on an ABM app while the account is NOT configured for ABM.   
        Below is the list of all affected users and or accounts:\n"""
        # for each result in the results create a failure message in email body
        for result in results:
            email_body += self.createFailureMessage(result[0], result[1], result[2])
            email_body += "\n\n"
        # set the content of message
        msg.set_content(email_body)
        # send the message
        #print(msg.as_string())
        self.smtp_server.sendmail(email_from, email_list, msg.as_string())
        
    # executes the version test
    def executeVersionTest(self):
        # tell the cursor to execute query
        self.mycursor.execute(sql)
        myresult = self.mycursor.fetchall()
        # if the results exist send warnings
        if len(myresult) > 0: 
            self.sendWarning(myresult)

    # init the class members
    def __init__(self):
        self.mycursor = mydb.cursor()
        self.smtp_server = smtplib.SMTP("smtp.gmail.com", 587)
        self.smtp_server.starttls()
        self.smtp_server.login("updates@cellcontrol.com", "Jake1980!")
        self.executeVersionTest()


# execute script
abmTest = ABMVersionCheck()

